<?php

namespace Foresite\Event\Test;

use Foresite\Event;

class EventTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @dataProvider dataProviderConstructor
    */
    public function testConstructor(array $data)
    {
        $event = new Event($data);
        
        $this->assertTrue(is_string($event->contactId()));
        $this->assertEquals($event->contactId(), $data['contact_id']);
        
        $this->assertTrue(is_string($event->name()));
        $this->assertEquals($event->name(), $data['name']);
        
        $this->assertTrue(is_string($event->locationName()));
        $this->assertEquals($event->locationName(), $data['location_name']);
        
        $this->assertTrue(is_string($event->city()));
        $this->assertEquals($event->city(), $data['city']);
        
        $this->assertTrue(is_string($event->state()));
        $this->assertEquals($event->state(), strtoupper($data['state']));
    }

    public function dataProviderConstructor()
    {
        return array(
            array(array(
                'contact_id' => 'ASDA3DDA-1233E',
                'name' => 'Great Lakes Tournament',
                'location_name' => 'H.V.C.C.',
                'city' => 'Philadelphia',
                'state' => 'Pa',
            )),
            array(array(
                'contact_id' => '',
                'name' => '',
                'location_name' => '',
                'city' => 1234,
                'state' => '',
            )),
        );
    }
}

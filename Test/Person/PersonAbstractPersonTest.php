<?php

namespace Foresite\Person\AbstractPerson\Test;

class PersonAbstractPersonTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @dataProvider dataProviderConstructor
    */
    public function testConstructor(array $data, $expecting)
    {
        
        $mock = $this->getMockBuilder('Foresite\Person\AbstractPerson')
                ->setConstructorArgs(array($data))
                ->getMockForAbstractClass();
        
        $this->assertTrue(is_string($mock->contactId()));
        $this->assertEquals($mock->contactId(), $expecting['contact_id']);
    
        $this->assertTrue(is_string($mock->firstName()));
        $this->assertEquals($mock->firstName(), $expecting['first_name']);
    
        $this->assertTrue(is_string($mock->lastName()));
        $this->assertEquals($mock->lastName(), $expecting['last_name']);
    
        $this->assertTrue(is_string($mock->email()));
        $this->assertEquals($mock->email(), $expecting['email']);
    
    }
    
    public function dataProviderConstructor()
    {
        return array(
            array(
                array(
                    'contact_id'      => '  AABBCC-123123-EEWWDD',
                    'first_name'      => ' Brett ',
                    'last_name'       => 'Santore  ',
                    'email'           => ' santore@usholeinone.com',
                    'phone_number'    => '215-840-0149',
                    'phone_extension' => '116',
                    'company'         => 'US Hole In One ',
                    
                ),
                array(
                    'contact_id'      => 'AABBCC-123123-EEWWDD',
                    'first_name'      => 'Brett',
                    'last_name'       => 'Santore',
                    'email'           => 'santore@usholeinone.com',
                    'phone_number'    => '215-840-0149',
                    'phone_extension' => '116',
                    'company'         => 'US Hole In One',
                )
            )
        );
    }

    /**
    * @dataProvider dataProviderFullName
    */
    public function testFullName($first_name, $last_name, $expected)
    {
        
        $mock = $this->getMockBuilder('Foresite\Person\AbstractPerson')
                ->setConstructorArgs(array(array(
                    'first_name' => $first_name,
                    'last_name' => $last_name
                )))
                ->getMockForAbstractClass();

        $this->assertEquals($mock->fullName(), $expected);
    }

    public function dataProviderFullName()
    {
        return array(
            array('Brett', 'Santore            ', 'Brett Santore'),
            array('', ' Santore ', 'Santore'),
            array('Brett ', '', 'Brett'),
            array('', '', ''),
        );
    }
}

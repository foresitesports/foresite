<?php

namespace Foresite\Email\Filter\Test;

use Foresite\Email\Filter;
use Foresite\Person\Customer;

class EmailFilterTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->filter = new Filter;
    }
    
    public function testAdd()
    {
        
        $emails = array(
            'santore@usholeinone.com',
            'carson@google.com',
            'Esterhai@usholineone.com',
            'Brett.Santore@gmail.com',
        );
        
        foreach ($emails as $email) {
            $customer = new Customer(array(
                'email' => $email,
            ));
            
            $this->assertTrue($this->filter->add($customer));
        }
    }
    
    public function testAddFail()
    {
        
        $customer = new Customer(array(
            'email' => 'santore@usholeinone.com',
        ));
        $this->assertTrue($this->filter->add($customer));
        
        $customer = new Customer(array(
            'email' => 'carson@google.com',
        ));
        $this->assertTrue($this->filter->add($customer));
        
        // email is already in list
        $customer = new Customer(array(
            'email' => 'santore@usholeinone.com',
        ));
        $this->assertFalse($this->filter->add($customer));
    }
    
    public function testGetList()
    {

        $this->filter->add(new Customer(array(
            'email' => 'carson@google.com',
        )));
        

        $this->filter->add(new Customer(array(
            'email' => 'santore@usholeinone.com',
        )));
        
        
        $list = $this->filter->getList();
        sort($list);
        
        $this->assertEquals($list, array(
            'carson@google.com',
            'santore@usholeinone.com',
        ));
    }
}

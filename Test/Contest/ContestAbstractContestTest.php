<?php

namespace Foresite\Contest\AbstractContest\Test;

class ContestAbstractContestTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorFull()
    {
        $data = array(
            'contact_id'        => 'AAADDDSEEESS-EEWQQWEE',
            'date'              => '2004-03-01',
            'fee'               => 200,
            'prize_value'       => 25000,
            'prize_description' => 'New Car ',
        );
            
        $mock = $this->getMockBuilder('Foresite\Contest\AbstractContest')
                ->setConstructorArgs(array($data))
                ->getMockForAbstractClass();
        
        $this->assertTrue(is_string($mock->contactId()));
        $this->assertEquals($mock->contactId(), 'AAADDDSEEESS-EEWQQWEE');
        
        $this->assertEquals($mock->date('m-d-Y'), '03-01-2004');
        
        $this->assertTrue(is_float($mock->prizeValue()));
        $this->assertEquals($mock->prizeValue(), 25000.0);
        
        $this->assertTrue(is_string($mock->prizeDescription()));
        $this->assertEquals($mock->prizeDescription(), 'New Car');
        
        $this->assertTrue(is_float($mock->fee()));
        $this->assertEquals($mock->fee(), 200.0);
    
    }
    
    public function testConstructorPartial()
    {
            
        $mock = $this->getMockBuilder('Foresite\Contest\AbstractContest')
                ->setConstructorArgs(array($data))
                ->getMockForAbstractClass();
        
        $this->assertTrue(is_string($mock->contactId()));
        $this->assertEquals($mock->contactId(), '');
        
        $this->assertTrue(is_string($mock->fee()));
        $this->assertEquals($mock->fee(), '');
    
        $this->assertTrue(is_string($mock->prizeValue()));
        $this->assertEquals($mock->prizeValue(), '');
        
        $this->assertTrue(is_string($mock->prizeValue()));
        $this->assertEquals($mock->prizeDescription(), '');
    
    }
}

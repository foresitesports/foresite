<?php

namespace Foresite\Contest\Golf\HoleInOne\Test;

use Foresite\Contest\Golf\HoleInOne;

class ContestGolfHoleInOneTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorFull()
    {
        $data = array(
            'pros' => 3,
        );
        
        $contest = new HoleInOne($data);
        
        $this->assertTrue(is_string($contest->type()));
        $this->assertEquals($contest->type(), 'HoleInOne');
        
        $this->assertTrue(is_int($contest->pros()));
        $this->assertEquals($contest->pros(), 3);
        
    }
    
    public function testConstructorPartial()
    {
        $data = array();
        
        $contest = new HoleInOne($data);
        
        $this->assertTrue(is_string($contest->type()));
        $this->assertEquals($contest->type(), 'HoleInOne');
        
        $this->assertTrue(is_int($contest->pros()));
        $this->assertEquals($contest->pros(), 0);
        
    }
}

<?php

namespace Foresite\Contest\Golf\AbstractGolfContest\Test;

class ContestAbstractGolfContestTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorFull()
    {
        $data = array(
            'date'              => '2004-03-01 00:00:00',
            'amateurs'          => 144,
            'hole_location'     => ' 3 - East ',
            'distance'          => 165,
        );
            
        $mock = $this->getMockBuilder('Foresite\Contest\Golf\AbstractGolfContest')
                ->setConstructorArgs(array($data))
                ->getMockForAbstractClass();
        
        $this->assertEquals($mock->date('m-d-Y'), '03-01-2004');
        
        $this->assertTrue(is_int($mock->amateurs()));
        $this->assertEquals($mock->amateurs(), 144);
        
        $this->assertTrue(is_string($mock->holeLocation()));
        $this->assertEquals($mock->holeLocation(), '3 - East');
        
        $this->assertTrue(is_int($mock->distance()));
        $this->assertEquals($mock->distance(), 165);
    
    }
    
    public function testConstructorPartial()
    {
        $data = array(
            'date'              => '',
            'hole_location'     => ' 3 - East ',
            'distance'          => '165',
        );
            
        $mock = $this->getMockBuilder('Foresite\Contest\Golf\AbstractGolfContest')
                ->setConstructorArgs(array($data))
                ->getMockForAbstractClass();
        
        $this->assertEquals($mock->date('m-d-Y'), '');
        
        $this->assertTrue(is_string($mock->amateurs()));
        $this->assertEquals($mock->amateurs(), '');
        
        $this->assertTrue(is_string($mock->holeLocation()));
        $this->assertEquals($mock->holeLocation(), '3 - East');
        
        $this->assertTrue(is_int($mock->distance()));
        $this->assertEquals($mock->distance(), 165);
            
    }
}

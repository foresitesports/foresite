<?php

namespace Foresite\Contest\Golf\Putting\Test;

use Foresite\Contest\Golf\Putting;

class ContestGolfPuttingTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorFull()
    {
        $data = array(
            'date'              => '2004-03-01 00:00:00',
            'amateurs'          => 144,
            'hole_location'     => ' 3 - East ',
            'distance'          => 165,
            'prize_value'       => 25000,
            'prize_description' => 'New Car ',
            'fee'               => 200,
        );
        
        $contest = new Putting($data);
        
        $this->assertTrue(is_string($contest->type()));
        $this->assertEquals($contest->type(), 'Putting');
        
    }
}

<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite\Email;

use Foresite\Person\AbstractPerson;

/**
 * Filter Class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
class Filter
{
    /**
     * A list of emails adresses
     *
     * @var string
     */
    protected $emailList = array();
    
    /**
     * Add this person's email address to the list
     *
     * @param AbstractPerson $person
     * @return BOOL
     * @author Brett Santore
     */
    public function add(AbstractPerson $person)
    {
        $email = strtolower($person->email());
        
        if (!in_array($email, $this->emailList)) {
            $this->emailList[] = $email;
            return true;
        }
        
        return false;
    }
    
    /**
     * Returns a array of email adresses
     *
     * @return void
     * @author Brett Santore
     */
    public function getList()
    {
        return $this->emailList;
    }
}

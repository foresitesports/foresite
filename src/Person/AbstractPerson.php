<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite\Person;

/**
 * AbstractPerson Class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
abstract class AbstractPerson
{
    /**
     * ContactID
     *
     * @var string
     */
    protected $contact_id;
    
    /**
     * First Name
     *
     * @var string
     */
    protected $first_name;
    
    /**
     * Last Name
     *
     * @var string
     */
    protected $last_name;
    
    /**
     * Email Address
     *
     * @var string
     */
    protected $email;
    
    /**
     * Phone Number
     *
     * @var string
     */
    protected $phone_number;
    
    /**
     * Phone Extension
     *
     * @var string
     */
    protected $phone_extension;
    
    /**
     * Company Name
     *
     * @var string
     */
    protected $company;
    
    /**
     * Constructor method
     *
     * @param array $data
     * @author Brett Santore
     */
    public function __construct($data = array())
    {
        $this->contact_id      = (isset($data['contact_id'])) ? (string) trim($data['contact_id']) : '';
        $this->first_name      = (isset($data['first_name'])) ? (string) trim($data['first_name']) : '';
        $this->last_name       = (isset($data['last_name'])) ? (string) trim($data['last_name']) : '';
        $this->email           = (isset($data['email'])) ? (string) trim($data['email']) : '';
        $this->phone_number    = (isset($data['phone_number'])) ? (string) trim($data['phone_number']) : '';
        $this->phone_extension = (isset($data['phone_extension'])) ? (string) trim($data['phone_extension']) : '';
        $this->company         = (isset($data['company'])) ? (string) trim($data['company']) : '';
    }
    
    /**
     * Get ID
     *
     * @return string
     * @author Brett Santore
     */
    public function contactId()
    {
        return $this->contact_id;
    }
    
    /**
     * Get first name
     *
     * @return string
     * @author Brett Santore
     */
    public function firstName()
    {
        return $this->first_name;
    }
    
    /**
     * Get last name
     *
     * @return void
     * @author Brett Santore
     */
    public function lastName()
    {
        return $this->last_name;
    }
    
    /**
     * Returns the fullname of the person with parts that are set
     *
     * @return string
     * @author Brett Santore
     */
    public function fullName()
    {
        if (empty($this->last_name)) {
            return $this->first_name;
        } elseif (empty($this->first_name)) {
            return $this->last_name;
        }
        
        return $this->first_name . ' ' . $this->last_name;
    }
    
    /**
     * Get email
     *
     * @return string
     * @author Brett Santore
     */
    public function email()
    {
        return $this->email;
    }
    
    /**
     * Get phone number
     *
     * @return string
     * @author Brett Santore
     */
    public function phoneNumber()
    {
        return $this->phone_number;
    }
    
    /**
     * Returns the phone number of the person
     *
     * @param string $format
     * @return void
     * @author Brett Santore
     */
    public function phoneExtension($format = 'Ext. ')
    {
        if (empty($this->phone_extension)) {
            return '';
        }
        
        return $format . $this->phone_extension;
    }
    
    /**
     * Get company name
     *
     * @return string
     * @author Brett Santore
     */
    public function company()
    {
        return $this->company;
    }
}

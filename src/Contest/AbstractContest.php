<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite\Contest;

use DateTimeImmutable;
use ReflectionClass;

/**
 * AbstractContest Class
 *
 * The base class for any contest class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
abstract class AbstractContest
{
    /**
     * The contact ID for the contest
     *
     * @var string
     */
    protected $contact_id;
    
    /**
     * Returns the contest date
     *
     * @var DateTimeImmutable|''
     */
    protected $date;
    
    /**
     * The prize value being covered
     *
     * @var float|string
     */
    protected $prize_value;
    
    /**
     * A description of the prize for the contest hole
     *
     * @var string
     */
    protected $prize_description;
    
    /**
     * The contest fee
     *
     * @var string
     */
    protected $fee;
    
    /**
     * Constructor method
     *
     * @param array $data
     * @author Brett Santore
     */
    public function __construct($data = array())
    {
        $this->contact_id        = (isset($data['contact_id'])) ? (string) trim($data['contact_id']) : '' ;
        $this->prize_value       = (isset($data['prize_value'])) ? (float) trim($data['prize_value']) : '' ;
        $this->prize_description = (isset($data['prize_description'])) ? (string) trim($data['prize_description']) : '' ;
        $this->fee               = (isset($data['fee'])) ? (float) trim($data['fee']) : '' ;
        
        if (isset($data['date']) && !empty($data['date'])) {
            $date = new DateTimeImmutable($data['date']);
            $date = $date->setTime(0, 0, 0);
            $this->date = $date;
        } else {
            $this->date = '';
        }
        
    }
    
    /**
     * Returns the contest type
     *
     * @return string
     * @author Brett Santore
     */
    public function type()
    {
        $class = new ReflectionClass($this);
        return $class->getShortName();
    }
    
    /**
     * Returns the contest id
     *
     * @return string
     * @author Brett Santore
     */
    public function contactID()
    {
        return $this->contact_id;
    }

    /**
     * Returns the contest date
     *
     * If not reset, returns empty string
     *
     * @param string $format
     * @return DateTimeImmutable|string
     * @author Brett Santore
     */
    public function date($format = 'm/d/Y')
    {
        if (!empty($this->date)) {
            return $this->date->format($format);
        }
        return '';
    }
    
    /**
     * Return the value of the prize
     *
     * @return float|string
     * @author Brett Santore
     */
    public function prizeValue()
    {
        return $this->prize_value;
    }
    
    /**
     * Returns the prize description
     *
     * @return string
     * @author Brett Santore
     */
    public function prizeDescription()
    {
        return $this->prize_description;
    }
    
    /**
     * Returns the contest fee
     *
     * @return float
     * @author Brett Santore
     */
    public function fee()
    {
        return $this->fee;
    }
}

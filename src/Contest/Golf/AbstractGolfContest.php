<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite\Contest\Golf;

use Foresite\Contest\AbstractContest;
use DateTimeImmutable;
use ReflectionClass;

/**
 * AbstractGolfClass
 *
 * The base class for any golf contest class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
abstract class AbstractGolfContest extends AbstractContest
{
    
    /**
     * Number of amateurs
     *
     * @var int|string
     */
    protected $amateurs;
    
    /**
     * The contest hole location
     *
     * @var string
     */
    protected $hole_location;
    
    /**
     * The contest distance in yards
     *
     * @var int|string
     */
    protected $distance;
    
    /**
     * {@inheritdoc}
     *
     * @param string $data
     * @author Brett Santore
     */
    public function __construct($data = array())
    {
        parent::__construct($data);

        $this->hole_location = (isset($data['hole_location'])) ? (string) trim($data['hole_location']) : '' ;
        $this->amateurs      = (isset($data['amateurs'])) ? (int) trim($data['amateurs']) : '' ;
        $this->distance      = (isset($data['distance'])) ? (int) trim($data['distance']) : '' ;
    }
    
    /**
     * Return the hole location
     *
     * @return string
     * @author Brett Santore
     */
    public function holeLocation()
    {
        return $this->hole_location;
    }
    
    /**
     * Return the number of amateurs
     *
     * @return int|string
     * @author Brett Santore
     */
    public function amateurs()
    {
        return $this->amateurs;
    }
    
    /**
     * Get the hole distance
     *
     * @return int|string
     * @author Brett Santore
     */
    public function distance()
    {
        return $this->distance;
    }
}

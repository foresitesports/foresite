<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite\Contest\Golf;

/**
 * Separator Class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
class Separator
{
    /**
     * Take an array of contests returned from the current database
     * and return and array of individual contests
     *
     * @param array $contests
     * @return void
     * @author Brett Santore
     */
    public static function separate(array $contests)
    {
        $self_data = array();
        
        if (isset($contests['contact_id'])) {
            unset($contests['contact_id']);
        }
        
        foreach ($contests as $key => $value) {
            $number = rtrim(substr($key, -1), '_');
            $property = substr($key, 2, -2);
            $type = rtrim(substr($key, 0, 2), '_');
            $new_contests[$type][$number][$property] = $value;
        }
        
        foreach ($new_contests as $contest_type => $the_contests) {
            foreach ($the_contests as $data) {
                if (0 == array_sum($data)) {
                    continue;
                }
                switch ($contest_type) {
                    case 'h':
                        $contest = new HoleInOne($data);
                        $ret[$contest->type()][] = $contest;
                        break;
                
                    case 'p':
                        $contest = new Putting($data);
                        $ret[$contest->type()][] = $contest;
                        break;
                
                    case 's':
                        $contest = new Shootout($data);
                        $ret[$contest->type()][] = $contest;
                        break;
                
                    default:
                        throw new \InvalidArgumentException("Unknown Golf Contest Type {$contest_type}");
                    
                        break;
                }
            }
            
        }
        
        return $ret;
    }
}

<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace  Foresite\Contest\Golf;

use Foresite\Contest\Golf\AbstractGolfContest;

/**
 * HoleInOne Class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
class HoleInOne extends AbstractGolfContest
{
    /**
     * The number of pros attempting the contest
     *
     * @var string
     */
    protected $pros;
    
    /**
     * {@inheritdoc}
     *
     * @param string $data
     * @author Brett Santore
     */
    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->pros = (isset($data['pros'])) ? (int) trim($data['pros']) : 0 ;
    }
    
    /**
     * Returns the number of pros attempting the contest
     *
     * @return int
     * @author Brett Santore
     */
    public function pros()
    {
        return $this->pros;
    }
}

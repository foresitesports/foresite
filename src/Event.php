<?php
/**
 * Foresite
 *
 * This content is released under the GNU General Public License, version 3 (GPL-3.0)
 *
 * Copyright (c) 2015, Foresite Sports
 *
 * @author  Brett Santpre
 * @copyright   Copyright (c) 2015, Foresite Sports. (http://www.foresitesports.com/)
 * @license     http://opensource.org/licenses/GPL-3.0  GPL-3.0
 * @link    https://bitbucket.org/foresitesports/foresite
 * @since   Version 1.0.0
 */

namespace Foresite;

/**
 * Event Class
 *
 * @author      Brett Santore
 * @link        https://bitbucket.org/foresitesports/foresite
 */
class Event
{
    /**
     * The Contact ID of the event
     *
     * @var string
     */
    protected $contact_id;
    
    /**
     * Name of the event
     *
     * @var string
     */
    protected $name;
    
    /**
     * The name of the location the event is held
     *
     * @var string
     */
    protected $location_name;
    
    /**
     * The city where the event is being held
     *
     * @var string
     */
    protected $city;
    
    /**
     * The state where the city is being held
     *
     * @var string
     */
    protected $state;
    
    /**
     * Constructor method
     *
     * @param array $data
     * @author Brett Santore
     */
    public function __construct($data = array())
    {
        $this->contact_id    = (isset($data['contact_id'])) ? (string) trim($data['contact_id']) : '';
        $this->name          = (isset($data['name'])) ? (string) trim($data['name']) : '';
        $this->location_name = (isset($data['location_name'])) ? (string) trim($data['location_name']) : '';
        $this->city          = (isset($data['city'])) ? (string) trim($data['city']) : '';
        $this->state         = (isset($data['state'])) ? strtoupper((string) trim($data['state'])) : '';
    }
    
    /**
     * Returns the contact id of the event
     *
     * @return string
     * @author Brett Santore
     */
    public function contactId()
    {
        return $this->contact_id;
    }

    /**
     * Returns the name of the event
     *
     * @return string
     * @author Brett Santore
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Returns the name of the location where the event is being held
     *
     * @return string
     * @author Brett Santore
     */
    public function locationName()
    {
        return $this->location_name;
    }

    /**
     * Returns the city in which the event is being help
     *
     * @return string
     * @author Brett Santore
     */
    public function city()
    {
        return $this->city;
    }

    /**
     * Returns the state in which the event is being help
     *
     * @return string
     * @author Brett Santore
     */
    public function state()
    {
        return $this->state;
    }
}
